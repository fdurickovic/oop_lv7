﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace OOP_LV7 {

	public partial class frmMain : Form {
		public frmMain()
			=> InitializeComponent();

		private string player1Name;
		private string player2Name;
		private int player1Score = 0;
		private int player2Score = 0;
		private Player turn;
		private PictureBox[] pictureBoxes;
		private bool isPlaying = false;

		private void FrmMain_Load(object sender, EventArgs e) {
			player1Name = Interaction.InputBox("Unesi ime prvog igrača:", "Unos", "Mirko");
			player2Name = Interaction.InputBox("Unesi ime drugog igrača:", "Unos", "Slavko");
			lblPlayer1Name.Text = lblPlayer1Name.Text.Replace("{igrac1}", player1Name);
			lblPlayer2Name.Text = lblPlayer2Name.Text.Replace("{igrac2}", player2Name);

			pictureBoxes = tableLayoutPanel1
							.Controls
							.OfType<PictureBox>()
							.OrderBy(s => s.Name)
							.ToArray();
			ResetGame();
		}

		private void ResetGame() {
			foreach (PictureBox pb in pictureBoxes) {
				pb.Tag = String.Empty;
				pb.CreateGraphics().Clear(Color.White);
			}
		}

		private void DisplayTurn()
			=> lblTurn.Text = turn == Player.X
								? $"{player1Name} (X)"
								: $"{player2Name} (O)";

		private void PictureBox_Click(object sender, EventArgs e) {
			if (sender is PictureBox pb) {
				if (String.IsNullOrEmpty((string)pb.Tag) && isPlaying) {
					Graphics g = pb.CreateGraphics();
					string currentTurn = turn == Player.X ? "X" : "O";
					g.DrawString(currentTurn,
								 new Font("Arial", 60, GraphicsUnit.Point),
								 new SolidBrush(Color.Black),
								 5,
								 5);
					pb.Tag = currentTurn;
					turn = turn == Player.X ? Player.O : Player.X;
					DisplayTurn();
					CheckWinner();
					g.Dispose();
				}
			}
		}

		private void CheckWinner() {
			//Diagonals
			if (((string)pictureBoxes[0].Tag == "X"
					&& (string)pictureBoxes[4].Tag == "X"
					&& (string)pictureBoxes[8].Tag == "X")
				|| ((string)pictureBoxes[2].Tag == "X"
					&& (string)pictureBoxes[4].Tag == "X"
					&& (string)pictureBoxes[6].Tag == "X")
				|| ((string)pictureBoxes[0].Tag == "X"
					&& (string)pictureBoxes[1].Tag == "X"
					&& (string)pictureBoxes[2].Tag == "X")
				|| ((string)pictureBoxes[3].Tag == "X"
					&& (string)pictureBoxes[4].Tag == "X"
					&& (string)pictureBoxes[5].Tag == "X")
				|| ((string)pictureBoxes[6].Tag == "X"
					&& (string)pictureBoxes[7].Tag == "X"
					&& (string)pictureBoxes[8].Tag == "X")
				|| ((string)pictureBoxes[0].Tag == "X"
					&& (string)pictureBoxes[3].Tag == "X"
					&& (string)pictureBoxes[6].Tag == "X")
				|| ((string)pictureBoxes[1].Tag == "X"
					&& (string)pictureBoxes[4].Tag == "X"
					&& (string)pictureBoxes[7].Tag == "X")
				|| ((string)pictureBoxes[2].Tag == "X"
					&& (string)pictureBoxes[5].Tag == "X"
					&& (string)pictureBoxes[8].Tag == "X")) {
				Player1Victory();
			} else
			if (((string)pictureBoxes[0].Tag == "O"
					&& (string)pictureBoxes[4].Tag == "O"
					&& (string)pictureBoxes[8].Tag == "O")
				|| ((string)pictureBoxes[2].Tag == "O"
					&& (string)pictureBoxes[4].Tag == "O"
					&& (string)pictureBoxes[6].Tag == "O")
				|| ((string)pictureBoxes[0].Tag == "O"
					&& (string)pictureBoxes[1].Tag == "O"
					&& (string)pictureBoxes[2].Tag == "O")
				|| ((string)pictureBoxes[3].Tag == "O"
					&& (string)pictureBoxes[4].Tag == "O"
					&& (string)pictureBoxes[5].Tag == "O")
				|| ((string)pictureBoxes[6].Tag == "O"
					&& (string)pictureBoxes[7].Tag == "O"
					&& (string)pictureBoxes[8].Tag == "O")
				|| ((string)pictureBoxes[0].Tag == "O"
					&& (string)pictureBoxes[3].Tag == "O"
					&& (string)pictureBoxes[6].Tag == "O")
				|| ((string)pictureBoxes[1].Tag == "O"
					&& (string)pictureBoxes[4].Tag == "O"
					&& (string)pictureBoxes[7].Tag == "O")
				|| ((string)pictureBoxes[2].Tag == "O"
					&& (string)pictureBoxes[5].Tag == "O"
					&& (string)pictureBoxes[8].Tag == "O")) {
				Player2Victory();
			} else
			if ((string)pictureBoxes[0].Tag != ""
				&& (string)pictureBoxes[1].Tag != ""
				&& (string)pictureBoxes[2].Tag != ""
				&& (string)pictureBoxes[3].Tag != ""
				&& (string)pictureBoxes[4].Tag != ""
				&& (string)pictureBoxes[5].Tag != ""
				&& (string)pictureBoxes[6].Tag != ""
				&& (string)pictureBoxes[7].Tag != ""
				&& (string)pictureBoxes[8].Tag != "") {
				MessageBox.Show("Tied");
				isPlaying = false;
				btnStart.Enabled = true;
			}
			lblPlayer1Score.Text = player1Score.ToString();
			lblPlayer2Score.Text = player2Score.ToString();
		}

		private void Player1Victory() {
			MessageBox.Show($"Igrač {player1Name} je pobjedio");
			player1Score++;
			isPlaying = false;
			btnStart.Enabled = true;
		}

		private void Player2Victory() {
			MessageBox.Show($"Igrač {player2Name} je pobjedio");
			player2Score++;
			isPlaying = false;
			btnStart.Enabled = true;
		}

		private void btnStart_Click(object sender, EventArgs e) {
			ResetGame();
			isPlaying = true;
			btnStart.Enabled = false;
			lblPlayer1Score.Text = player1Score.ToString();
			lblPlayer2Score.Text = player2Score.ToString();
		}
	}
}