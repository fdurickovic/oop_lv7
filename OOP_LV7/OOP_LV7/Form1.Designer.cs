﻿namespace OOP_LV7 {
	partial class frmMain {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.lblTurn = new System.Windows.Forms.Label();
			this.lblPlayer1Name = new System.Windows.Forms.Label();
			this.lblPlayer2Name = new System.Windows.Forms.Label();
			this.btnStart = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.pbField8 = new System.Windows.Forms.PictureBox();
			this.pbField7 = new System.Windows.Forms.PictureBox();
			this.pbField6 = new System.Windows.Forms.PictureBox();
			this.pbField5 = new System.Windows.Forms.PictureBox();
			this.pbField4 = new System.Windows.Forms.PictureBox();
			this.pbField3 = new System.Windows.Forms.PictureBox();
			this.pbField2 = new System.Windows.Forms.PictureBox();
			this.pbField1 = new System.Windows.Forms.PictureBox();
			this.pbField0 = new System.Windows.Forms.PictureBox();
			this.lblPlayer1Score = new System.Windows.Forms.Label();
			this.lblPlayer2Score = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbField8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField0)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(364, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(67, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "Na redu je:";
			// 
			// lblTurn
			// 
			this.lblTurn.AutoSize = true;
			this.lblTurn.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
			this.lblTurn.Location = new System.Drawing.Point(437, 14);
			this.lblTurn.Name = "lblTurn";
			this.lblTurn.Size = new System.Drawing.Size(0, 15);
			this.lblTurn.TabIndex = 2;
			// 
			// lblPlayer1Name
			// 
			this.lblPlayer1Name.AutoSize = true;
			this.lblPlayer1Name.Location = new System.Drawing.Point(364, 77);
			this.lblPlayer1Name.Name = "lblPlayer1Name";
			this.lblPlayer1Name.Size = new System.Drawing.Size(100, 15);
			this.lblPlayer1Name.TabIndex = 3;
			this.lblPlayer1Name.Text = "Igrač {igrac1} (X):";
			// 
			// lblPlayer2Name
			// 
			this.lblPlayer2Name.AutoSize = true;
			this.lblPlayer2Name.Location = new System.Drawing.Point(364, 92);
			this.lblPlayer2Name.Name = "lblPlayer2Name";
			this.lblPlayer2Name.Size = new System.Drawing.Size(102, 15);
			this.lblPlayer2Name.TabIndex = 4;
			this.lblPlayer2Name.Text = "Igrač {igrac2} (O):";
			// 
			// btnStart
			// 
			this.btnStart.BackColor = System.Drawing.SystemColors.Control;
			this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnStart.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
			this.btnStart.Location = new System.Drawing.Point(3, 3);
			this.btnStart.Margin = new System.Windows.Forms.Padding(0);
			this.btnStart.Name = "btnStart";
			this.btnStart.Size = new System.Drawing.Size(67, 27);
			this.btnStart.TabIndex = 5;
			this.btnStart.Text = "Start";
			this.btnStart.UseVisualStyleBackColor = false;
			this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
			// 
			// panel1
			// 
			this.panel1.AutoSize = true;
			this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.btnStart);
			this.panel1.Location = new System.Drawing.Point(364, 323);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(3);
			this.panel1.Size = new System.Drawing.Size(77, 37);
			this.panel1.TabIndex = 6;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Controls.Add(this.pbField8, 2, 2);
			this.tableLayoutPanel1.Controls.Add(this.pbField7, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.pbField6, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.pbField5, 2, 1);
			this.tableLayoutPanel1.Controls.Add(this.pbField4, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.pbField3, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.pbField2, 2, 0);
			this.tableLayoutPanel1.Controls.Add(this.pbField1, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.pbField0, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 14);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(346, 346);
			this.tableLayoutPanel1.TabIndex = 11;
			// 
			// pbField8
			// 
			this.pbField8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField8.Location = new System.Drawing.Point(234, 234);
			this.pbField8.Name = "pbField8";
			this.pbField8.Size = new System.Drawing.Size(108, 108);
			this.pbField8.TabIndex = 8;
			this.pbField8.TabStop = false;
			this.pbField8.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField7
			// 
			this.pbField7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField7.Location = new System.Drawing.Point(119, 234);
			this.pbField7.Name = "pbField7";
			this.pbField7.Size = new System.Drawing.Size(108, 108);
			this.pbField7.TabIndex = 7;
			this.pbField7.TabStop = false;
			this.pbField7.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField6
			// 
			this.pbField6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField6.Location = new System.Drawing.Point(4, 234);
			this.pbField6.Name = "pbField6";
			this.pbField6.Size = new System.Drawing.Size(108, 108);
			this.pbField6.TabIndex = 6;
			this.pbField6.TabStop = false;
			this.pbField6.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField5
			// 
			this.pbField5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField5.Location = new System.Drawing.Point(234, 119);
			this.pbField5.Name = "pbField5";
			this.pbField5.Size = new System.Drawing.Size(108, 108);
			this.pbField5.TabIndex = 5;
			this.pbField5.TabStop = false;
			this.pbField5.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField4
			// 
			this.pbField4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField4.Location = new System.Drawing.Point(119, 119);
			this.pbField4.Name = "pbField4";
			this.pbField4.Size = new System.Drawing.Size(108, 108);
			this.pbField4.TabIndex = 4;
			this.pbField4.TabStop = false;
			this.pbField4.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField3
			// 
			this.pbField3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField3.Location = new System.Drawing.Point(4, 119);
			this.pbField3.Name = "pbField3";
			this.pbField3.Size = new System.Drawing.Size(108, 108);
			this.pbField3.TabIndex = 3;
			this.pbField3.TabStop = false;
			this.pbField3.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField2
			// 
			this.pbField2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField2.Location = new System.Drawing.Point(234, 4);
			this.pbField2.Name = "pbField2";
			this.pbField2.Size = new System.Drawing.Size(108, 108);
			this.pbField2.TabIndex = 2;
			this.pbField2.TabStop = false;
			this.pbField2.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField1
			// 
			this.pbField1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField1.Location = new System.Drawing.Point(119, 4);
			this.pbField1.Name = "pbField1";
			this.pbField1.Size = new System.Drawing.Size(108, 108);
			this.pbField1.TabIndex = 1;
			this.pbField1.TabStop = false;
			this.pbField1.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// pbField0
			// 
			this.pbField0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pbField0.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pbField0.Location = new System.Drawing.Point(4, 4);
			this.pbField0.Name = "pbField0";
			this.pbField0.Size = new System.Drawing.Size(108, 108);
			this.pbField0.TabIndex = 0;
			this.pbField0.TabStop = false;
			this.pbField0.Click += new System.EventHandler(this.PictureBox_Click);
			// 
			// lblPlayer1Score
			// 
			this.lblPlayer1Score.AutoSize = true;
			this.lblPlayer1Score.Location = new System.Drawing.Point(470, 77);
			this.lblPlayer1Score.Name = "lblPlayer1Score";
			this.lblPlayer1Score.Size = new System.Drawing.Size(0, 15);
			this.lblPlayer1Score.TabIndex = 12;
			// 
			// lblPlayer2Score
			// 
			this.lblPlayer2Score.AutoSize = true;
			this.lblPlayer2Score.Location = new System.Drawing.Point(472, 92);
			this.lblPlayer2Score.Name = "lblPlayer2Score";
			this.lblPlayer2Score.Size = new System.Drawing.Size(0, 15);
			this.lblPlayer2Score.TabIndex = 13;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(562, 372);
			this.Controls.Add(this.lblPlayer2Score);
			this.Controls.Add(this.lblPlayer1Score);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.lblPlayer2Name);
			this.Controls.Add(this.lblPlayer1Name);
			this.Controls.Add(this.lblTurn);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Arial", 9F);
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.Text = "Križić-kružić";
			this.Load += new System.EventHandler(this.FrmMain_Load);
			this.panel1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pbField8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pbField0)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblTurn;
		private System.Windows.Forms.Label lblPlayer1Name;
		private System.Windows.Forms.Label lblPlayer2Name;
		private System.Windows.Forms.Button btnStart;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.PictureBox pbField8;
		private System.Windows.Forms.PictureBox pbField7;
		private System.Windows.Forms.PictureBox pbField6;
		private System.Windows.Forms.PictureBox pbField5;
		private System.Windows.Forms.PictureBox pbField4;
		private System.Windows.Forms.PictureBox pbField3;
		private System.Windows.Forms.PictureBox pbField2;
		private System.Windows.Forms.PictureBox pbField1;
		private System.Windows.Forms.PictureBox pbField0;
		private System.Windows.Forms.Label lblPlayer1Score;
		private System.Windows.Forms.Label lblPlayer2Score;
	}
}

